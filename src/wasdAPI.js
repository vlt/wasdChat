import {computed, watch} from 'vue'
import {useFetch, createFetch} from '@vueuse/core'
import _ from 'lodash'

export const useWASDFetch = (wasdToken, uri) => {
    return createFetch({
        baseUrl: 'https://wasd.tv',
        options: {
            async beforeFetch({options}) {
                options.headers.Authorization = `Authorization: Token ${wasdToken}`
                return {options}
            },
        },
    })(uri)
}

export const useGetChannelInfo = (channelName, wasdToken) => {
    const {
        execute,
        isFinished: channelInfoReady,
        data: channelInfoData
    } = useFetch(
        `https://wasd.tv/api/v2/broadcasts/public?channel_name=${channelName}`, {
            afterFetch(ctx) {
                if (!_.has(ctx.data, 'result.media_container.media_container_streams[0].stream_id')) {
                    setTimeout(() => {
                        execute()
                    }, 10000)
                }
                return ctx
            },
        }).get().json()

    const {
        isFinished: chatTokenReady,
        data: chatTokenData
    } = useWASDFetch(wasdToken, 'api/auth/chat-token').post().json()

    const streamInfoReady = computed(() => channelInfoReady.value &&_.has(chatTokenData.value, 'result') && _.has(channelInfoData.value, 'result.media_container.media_container_streams[0].stream_id'))
    const channelInfo = computed(() => _.get(channelInfoData.value, 'result', {}))
    const chatToken = computed(() => _.get(chatTokenData.value, 'result', ''))

    return {chatTokenReady, streamInfoReady, channelInfo, chatToken}
}